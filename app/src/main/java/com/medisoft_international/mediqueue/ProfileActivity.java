package com.medisoft_international.mediqueue;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.medisoft_international.mediqueue.AzureModels.Patient;
import com.medisoft_international.mediqueue.R;

public class ProfileActivity extends AppCompatActivity {
    Toolbar mToolbar;
    Button mLogout;
    FirebaseAuth mAuth;
    Patient mPatient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Profile");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mPatient = Util.GetPatient(this);
        mAuth = FirebaseAuth.getInstance();
        mLogout = (Button) findViewById(R.id.logoutButton);
        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.Log(mPatient);
                mAuth.signOut();
                SharedPreferences.Editor editor = Util.getSharedPref(ProfileActivity.this).edit();
                editor.remove(ProfileActivity.this.getString(R.string.shared_pref_pasien)).apply();
                Intent goToSplash = new Intent(ProfileActivity.this, SplashActivity.class);
                goToSplash.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(goToSplash);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

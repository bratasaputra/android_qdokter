package com.medisoft_international.mediqueue;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureModels.Patient;
import com.medisoft_international.mediqueue.AzureModels.Reservation;
import com.medisoft_international.mediqueue.VolleyRequest.MakeReservationRequest;
import com.medisoft_international.mediqueue.events.StickyReservationData;
import org.greenrobot.eventbus.EventBus;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class BookingSelectionConfirmationActivity extends AppCompatActivity {

    private StickyReservationData reservationData;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_selection_confirmation);

        reservationData = EventBus.getDefault().getStickyEvent(StickyReservationData.class);
        if (reservationData == null) {
            finish();
        } else {
            setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setTitle("Check kembali reservasi anda");
            }
            progressBar = (ProgressBar) findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
            TextView hospitalName = (TextView) findViewById(R.id.hospital_name);
            hospitalName.setText(reservationData.hospital.name);
            TextView serviceName = (TextView) findViewById(R.id.service_name);
            serviceName.setText(reservationData.service.name);
            SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
            int year = Integer.valueOf(reservationData.reservation.appointmentDate.substring(0, 4));
            int month = Integer.valueOf(reservationData.reservation.appointmentDate.substring(4, 6));
            int day = Integer.valueOf(reservationData.reservation.appointmentDate.substring(6, 8));
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day, reservationData.slot.startHour, reservationData.slot.startMinute, 0);
            TextView date = (TextView) findViewById(R.id.date);
            date.setText(format.format(calendar.getTime()));
            TextView time = (TextView) findViewById(R.id.time);
            time.setText(new SimpleDateFormat("HH : mm", Locale.getDefault()).format(calendar.getTime()));

            TextView additionalText = (TextView) findViewById(R.id.textView9);
            final EditText additionalContent = (EditText) findViewById(R.id.additional_info);
            if (reservationData.hospital.additionaldataname != null) {
                additionalText.setText(reservationData.hospital.additionaldataname);
                additionalText.setVisibility(View.VISIBLE);
                additionalContent.setVisibility(View.VISIBLE);
            } else {
                additionalText.setVisibility(View.GONE);
                additionalContent.setVisibility(View.GONE);
            }

            Button ok = (Button) findViewById(R.id.positive_button);
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Patient mPatient = new Gson()
                            .fromJson(getSharedPreferences(getString(R.string.shared_pref_name), MODE_PRIVATE)
                                            .getString(getString(R.string.shared_pref_pasien), null)
                                    , Patient.class);
                    progressBar.setVisibility(View.VISIBLE);
                    reservationData.reservation.patient = mPatient._id;
                    reservationData.reservation.additionaldataname = reservationData.hospital.additionaldataname;
                    reservationData.reservation.additionaldatacontent = additionalContent.getText().toString();

                    MakeReservationRequest request = new MakeReservationRequest(reservationData.reservation,
                            new Response.Listener<Reservation>() {
                                @Override
                                public void onResponse(Reservation response) {
                                    progressBar.setVisibility(View.GONE);
                                    Intent intent = new Intent(getApplicationContext(), BookingConfirmationActivity.class);
                                    intent.putExtra(Reservation.class.getSimpleName(), new Gson().toJson(response));
                                    startActivity(intent);
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressBar.setVisibility(View.GONE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(BookingSelectionConfirmationActivity.this);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builder.setTitle("Encounter an error");
                            switch (error.networkResponse.statusCode) {
                                case 403:
                                    builder.setMessage(R.string.reservation_one_per_day_limit_warning).show();
                                    break;
                                default:
                                    builder.setMessage(R.string.unable_to_make_reservation).show();
                                    break;
                            }
                        }
                    });
                    Medisoft.getDefaultRequestQueue().add(request);
                }
            });
            Button no = (Button) findViewById(R.id.negative_button);
            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

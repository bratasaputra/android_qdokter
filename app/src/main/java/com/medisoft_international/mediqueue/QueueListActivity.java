package com.medisoft_international.mediqueue;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.medisoft_international.mediqueue.AzureModels.QueueEntry;
import com.medisoft_international.mediqueue.VolleyRequest.GetQueueEntryRequest;
import com.medisoft_international.mediqueue.adapters.QueueAdapter;
import com.medisoft_international.mediqueue.events.BarcodeDetectedEvent;
import com.medisoft_international.mediqueue.events.DeleteItemEvent;
import com.medisoft_international.mediqueue.events.ItemClickEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import java.util.ArrayList;
import java.util.Calendar;
import io.realm.Realm;
import io.realm.RealmResults;

public class QueueListActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private QueueAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_queue_list);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.queue_list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new QueueAdapter(new ArrayList<QueueEntry>());
        mRecyclerView.setAdapter(mAdapter);

        FloatingActionButton mFab = (FloatingActionButton) findViewById(R.id.scan_qr_code_button);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startBarcodeScanner();
            }
        });
        String jsonPatient = getSharedPreferences(getString(R.string.shared_pref_name), MODE_PRIVATE)
                .getString(getString(R.string.shared_pref_pasien), null);
        if (jsonPatient == null) {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.queue_list_menu, menu);
        return true;
    }

    public void startBarcodeScanner() {
        startActivity(new Intent(this, BarcodeCaptureActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return true;
        } else if (item.getItemId() == R.id.scan_qr_code) {
            startBarcodeScanner();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        refresh();
    }

    public void refresh() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Realm realm = Realm.getDefaultInstance();
        RealmResults<QueueEntry> result = realm.where(QueueEntry.class)
                .greaterThanOrEqualTo("timestamp", calendar.getTime()).findAll();
        mAdapter.setDataSet(result);
        if (mAdapter.getItemCount() == 0) {
            findViewById(R.id.empty_queue).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.empty_queue).setVisibility(View.GONE);
        }
    }

    @Subscribe(sticky = true)
    public void onBarcodeDetected(BarcodeDetectedEvent event) {
        String url = event.barcode.displayValue;
        final String[] tokens = url.split(":");
        if (tokens.length == 2) {
            final Snackbar indicator = Snackbar.make(mRecyclerView, "Tunggu Sebentar...", Snackbar.LENGTH_INDEFINITE);
            indicator.show();
            GetQueueEntryRequest request = new GetQueueEntryRequest(tokens[1],
                    new Response.Listener<QueueEntry>() {
                        @Override
                        public void onResponse(QueueEntry response) {
                            indicator.dismiss();
                            Realm realm = Realm.getDefaultInstance();
                            realm.beginTransaction();
                            realm.copyToRealmOrUpdate(response);
                            realm.commitTransaction();
                            refresh();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Snackbar.make(mRecyclerView, R.string.unable_to_connect_to_server, Snackbar.LENGTH_LONG).show();
                }
            });
            Medisoft.getDefaultRequestQueue().add(request);
        } else {
            Snackbar.make(mRecyclerView, "Nomor antrian tidak ditemukan", Snackbar.LENGTH_LONG).show();
        }
        EventBus.getDefault().removeStickyEvent(BarcodeDetectedEvent.class);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Subscribe
    public void OnItemClick(ItemClickEvent<QueueEntry> event) {
        CheckQueueAndWatchAdFragment fragment = CheckQueueAndWatchAdFragment.Create(event.item);
        fragment.show(getSupportFragmentManager(), "DI");
    }

    @Subscribe
    public void OnItemDelete(DeleteItemEvent<QueueEntry> event) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        event.item.deleteFromRealm();
        realm.commitTransaction();
        refresh();
    }
}

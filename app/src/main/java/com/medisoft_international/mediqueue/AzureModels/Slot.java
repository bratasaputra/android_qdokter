package com.medisoft_international.mediqueue.AzureModels;

/**
 * Created by Medisoft Development on 2017-07-03.
 */

public class Slot {
    public String _id;
    public String service = null;
    public String category = null;
    public String hospital = null;
    public int wave = 0;
    public int day = 0;
    public int startHour = 0;
    public int startMinute = 0;
    public int endHour = 23;
    public int endMinute = 59;
    public boolean overrideQuota = false;
    public Integer remainingQuota = null;
}

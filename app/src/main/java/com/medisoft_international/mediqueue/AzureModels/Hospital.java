package com.medisoft_international.mediqueue.AzureModels;

/**
 * Created by Medisoft Development on 2017-06-03.
 */

public class Hospital {
    public String _id;
    public String name;
    public String address;
    public String image;
    public Double longitude;
    public Double latitude;
    public Double meta_jarak;
    public Double meta_score;
    public String additionaldataname;
}

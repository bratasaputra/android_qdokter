package com.medisoft_international.mediqueue.AzureModels;

/**
 * Created by Medisoft Development on 2017-06-03.
 */

public class Specialisation {
    public String _id;
    public String name;
    public Double meta_score;

    @Override
    public String toString() {
        return this.name;
    }
}

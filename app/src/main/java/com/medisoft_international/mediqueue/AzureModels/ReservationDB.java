package com.medisoft_international.mediqueue.AzureModels;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Medisoft Development on 2017-10-03.
 */

public class ReservationDB extends RealmObject {
    @PrimaryKey
    public String _id;
    public String slot;
    public String category;
    public String service;
    public String spesialisation;
    public String patient;
    public String appointmentDate;
    public String appointmentTime;
    public String bookingCode;
    public Boolean retrieved;
    public String hospital;

    public String hospitalName;
    public String serviceName;

    public ReservationDB create(Reservation r) {
        _id = r._id;
        slot = r.slot;
        category = r.category;
        service = r.service;
        spesialisation = r.spesialisation;
        patient = r.patient;
        appointmentDate = r.appointmentDate;
        appointmentTime = r.appointmentTime;
        bookingCode = r.bookingCode;
        retrieved = r.retrieved;
        hospital = r.hospital;
        return this;
    }
}
package com.medisoft_international.mediqueue.AzureModels;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Medisoft Development on 2017-17-03.
 */

public class QueueEntry extends RealmObject {
    @PrimaryKey
    public String _id;
    public Integer entryId;
    public String qNumber;
    public Date timestamp;
    public String hospital;
    public String serviceName;

    public QueueEntry Create(QueueEntry entry) {
        this._id = entry._id;
        this.entryId = entry.entryId;
        this.qNumber = entry.qNumber;
        this.timestamp = entry.timestamp;
        this.hospital = entry.hospital;
        this.serviceName = entry.serviceName;
        return this;
    }
}

package com.medisoft_international.mediqueue.AzureModels;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Medisoft Development on 2017-07-03.
 */

public class Reservation extends RealmObject {
    @PrimaryKey
    public String _id;
    public String slot;
    public String category;
    public String service;
    public String spesialisation;
    public String patient;
    public String appointmentDate;
    public String appointmentTime;
    public String bookingCode;
    public Boolean retrieved;
    public String hospital;
    public String additionaldataname;
    public String additionaldatacontent;
    public String serviceName;
    public String hospitalName;
}

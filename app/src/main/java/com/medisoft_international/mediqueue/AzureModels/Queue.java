package com.medisoft_international.mediqueue.AzureModels;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Medisoft Development on 2017-08-03.
 */

public class Queue extends RealmObject {
    @PrimaryKey
    public String url;
    public String Time;
    public String HospitalId;
    public String ServiceId;
    public String Qnumber;
    public String Wave;

    @SerializedName("serviceName")
    public String ServiceName;

    public class QueueCount {
        public Integer Count;
    }
}

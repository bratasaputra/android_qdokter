package com.medisoft_international.mediqueue.AzureModels;

/**
 * Created by Medisoft Development on 2017-03-03.
 */

public class Patient {
    public String _id;
    public String name;
    public String address;
    public String postcode;
    public String dob;
    public String phone;
    public Boolean male;
    public Double meta_score;
}

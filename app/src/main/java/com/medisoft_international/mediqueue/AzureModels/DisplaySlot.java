package com.medisoft_international.mediqueue.AzureModels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Medisoft Development on 2017-13-03.
 */

public class DisplaySlot {
    public int dayOfWeek;
    public List<Slot> slotList = new ArrayList<>();
}

package com.medisoft_international.mediqueue.AzureModels;

/**
 * Created by Medisoft Development on 2017-07-03.
 */

public class Service {
    public String _id;
    public String name;
    public String description;
    public String category;
    public String spesialisation;
    public String hospital;
    public String doctor;
    public Boolean bpjs;
    public Double meta_score;
}

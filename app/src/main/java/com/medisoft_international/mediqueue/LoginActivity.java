package com.medisoft_international.mediqueue;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.medisoft_international.mediqueue.AzureModels.Patient;
import com.medisoft_international.mediqueue.R;
import com.medisoft_international.mediqueue.VolleyRequest.SearchPatientRequest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    Toolbar mToolbar;
    Button mLogin;
    EditText mPhone;
    ProgressBar mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Login");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mPhone = (EditText) findViewById(R.id.phoneNumber);
        mProgress = (ProgressBar) findViewById(R.id.progressBar);
        mProgress.setVisibility(View.GONE);
        mLogin = (Button) findViewById(R.id.loginButton);
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Validate phone number
                //Check phone number exist
                mLogin.setVisibility(View.GONE);
                mProgress.setVisibility(View.VISIBLE);
                String phoneNumber = Util.FormatPhoneNumber(mPhone.getText().toString());
                if (phoneNumber == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            mLogin.setVisibility(View.VISIBLE);
                            mProgress.setVisibility(View.GONE);
                        }
                    });
                    builder.setTitle("Invalid Phone Number");
                    builder.setMessage("Please insert a valid phone number");
                    builder.show();
                } else {
                    Map<String, String> searchParams = new HashMap<String, String>();
                    searchParams.put("phoneNumber", phoneNumber);
                    SearchPatientRequest request = new SearchPatientRequest(searchParams, new Response.Listener<List<Patient>>() {
                        @Override
                        public void onResponse(List<Patient> response) {
                            if (response.isEmpty()) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        finish();
                                    }
                                });
                                builder.setTitle("Phone Number Not Registered");
                                builder.setMessage("Please select registration to start using QDokter");
                                builder.show();
                            } else {
                                Util.StickPatient(response.get(0));
                                Intent goToVerification = new Intent(LoginActivity.this, PhoneVerificationActivity.class);
                                startActivity(goToVerification);
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    mLogin.setVisibility(View.VISIBLE);
                                    mProgress.setVisibility(View.GONE);
                                }
                            });
                            builder.setTitle("Unable to Retrieve Data");
                            builder.setMessage("Please try again later");
                            builder.show();
                        }
                    });
                    Medisoft.getDefaultRequestQueue().add(request);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

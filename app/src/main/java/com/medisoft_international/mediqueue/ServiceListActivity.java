package com.medisoft_international.mediqueue;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureModels.Hospital;
import com.medisoft_international.mediqueue.AzureModels.Service;
import com.medisoft_international.mediqueue.VolleyRequest.ServiceListRequest;
import com.medisoft_international.mediqueue.adapters.ServiceAdapter;
import com.medisoft_international.mediqueue.events.ItemClickEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServiceListActivity extends AppCompatActivity {
    protected Hospital mHospital;
    protected TextView mHospitalName;
    protected RecyclerView recyclerView;
    protected RecyclerView.LayoutManager layoutManager;
    protected ServiceAdapter adapter;
    protected ContentLoadingProgressBar progressBar;
    FloatingActionButton mRefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicelist);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        mRefresh = (FloatingActionButton) findViewById(R.id.refreshButton);
        mRefresh.setVisibility(View.GONE);
        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetch();
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mHospital = new Gson().fromJson(getIntent().getStringExtra(Hospital.class.getSimpleName()), Hospital.class);
        if (mHospital == null) {
            finish();
        } else {
            mHospitalName = (TextView) findViewById(R.id.hospital_name);
            mHospitalName.setText(mHospital.name);
            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
            layoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(layoutManager);
            adapter = new ServiceAdapter(new ArrayList<Service>());
            recyclerView.setAdapter(adapter);
            progressBar = (ContentLoadingProgressBar) findViewById(R.id.service_progress);
            fetch();
        }
    }

    private void hideLoading() {
        progressBar.hide();
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void showLoading() {
        progressBar.show();
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    private void fetch() {
        Medisoft.getDefaultRequestQueue().cancelAll(ServiceListRequest.class);
        Map<String, String> searchParams = new HashMap<>();
        searchParams.put("hospital", mHospital._id);
        ServiceListRequest request = new ServiceListRequest(searchParams, new Response.Listener<List<Service>>() {
            @Override
            public void onResponse(List<Service> response) {
                adapter.setDataSet(response);
                hideLoading();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Util.Log(error);
                hideLoading();
                mRefresh.setVisibility(View.VISIBLE);
                Snackbar.make(recyclerView, R.string.unable_to_load_service_list, Snackbar.LENGTH_LONG).show();
            }
        });
        showLoading();
        mRefresh.setVisibility(View.GONE);
        Medisoft.getDefaultRequestQueue().add(request);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void OnItemClick(ItemClickEvent<Service> event) {
        Intent intent = new Intent(this, SlotListActivity.class);
        intent.putExtra(Service.class.getSimpleName(), new Gson().toJson(event.item));
        startActivity(intent);
    }
}

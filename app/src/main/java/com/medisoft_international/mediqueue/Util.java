package com.medisoft_international.mediqueue;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.medisoft_international.mediqueue.AzureModels.Patient;

import org.greenrobot.eventbus.EventBus;

import static android.content.Context.MODE_PRIVATE;

public class Util {
    public static void Log(Object value) {
        if (value == null) {
            value = "NULL";
        }
        Log.d(BuildConfig.TAG, value.getClass() == String.class ?
                value.toString() : new Gson().toJson(value));
    }

    public static String FormatPhoneNumber(String phoneNumber) {
        PhoneNumberUtil util = PhoneNumberUtil.getInstance();
        try {
            Phonenumber.PhoneNumber formattedPhoneNumber = util.parse(phoneNumber, "ID");
            if (util.isValidNumberForRegion(formattedPhoneNumber, "ID")) {
                return util.format(formattedPhoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
            } else {
                return null;
            }
        }
        catch (NumberParseException e) {
            return null;
        }
    }

    public static SharedPreferences getSharedPref(Context context) {
        return context.getSharedPreferences(context.getString(R.string.shared_pref_name), MODE_PRIVATE);
    }

    public static String getStoredPatient(Context context) {
        SharedPreferences pref = context.getSharedPreferences(context.getString(R.string.shared_pref_name), MODE_PRIVATE);
        return pref.getString(context.getString(R.string.shared_pref_pasien), null);
    }

    public static void savePatient(Context context, Patient patient) {
        SharedPreferences pref = context.getSharedPreferences(context.getString(R.string.shared_pref_name), MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        Gson gson = new Gson();
        editor.putString(context.getString(R.string.shared_pref_pasien), gson.toJson(patient)).apply();
    }

    public static void StickPatient(Patient patient) {
        EventBus.getDefault().removeStickyEvent(Patient.class);
        if (patient != null) {
            EventBus.getDefault().postSticky(patient);
        }
    }

    public static Patient GetPatient(Context context) {
        Patient mPatient = EventBus.getDefault().getStickyEvent(Patient.class);
        if (mPatient == null) {
            String jsonPatient = getStoredPatient(context);
            mPatient = new Gson().fromJson(jsonPatient, Patient.class);
            StickPatient(mPatient);
        }
        return  mPatient;
    }
}

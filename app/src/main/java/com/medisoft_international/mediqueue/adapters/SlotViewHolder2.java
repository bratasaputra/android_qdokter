package com.medisoft_international.mediqueue.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.medisoft_international.mediqueue.AzureModels.DisplaySlot;
import com.medisoft_international.mediqueue.AzureModels.Slot;
import com.medisoft_international.mediqueue.R;
import com.medisoft_international.mediqueue.events.ItemClickEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

/**
 * Created by Medisoft Development on 2017-07-03.
 */

public class SlotViewHolder2 extends RecyclerView.ViewHolder {
    protected TextView mDay;
    protected LinearLayout mHead;
    protected Context mContext;

    public SlotViewHolder2(View view) {
        super(view);
        mContext = view.getContext();
        mDay = (TextView) view.findViewById(R.id.slot_day);
        mHead = (LinearLayout) view.findViewById(R.id.slot_content);
    }

    public void bind(DisplaySlot slot) {
        String day = mContext.getString(R.string.monday);
        switch (slot.dayOfWeek) {
            case 0:
                day = mContext.getString(R.string.sunday);
                break;
            case 1:
                day = mContext.getString(R.string.monday);
                break;
            case 2:
                day = mContext.getString(R.string.tuesday);
                break;
            case 3:
                day = mContext.getString(R.string.wednesday);
                break;
            case 4:
                day = mContext.getString(R.string.thursday);
                break;
            case 5:
                day = mContext.getString(R.string.friday);
                break;
            case 6:
                day = mContext.getString(R.string.saturday);
                break;
        }
        mDay.setText(day);
        for (Slot s :
                slot.slotList) {
            View children = LayoutInflater.from(mContext).inflate(R.layout.slot_option, mHead, false);
            TextView mStart = (TextView) children.findViewById(R.id.slot_start);
            TextView mEnd = (TextView) children.findViewById(R.id.slot_end);
            mStart.setText(String.format(Locale.getDefault(),
                    mContext.getString(R.string.slot_card_time_format), s.startHour, s.startMinute));
            mEnd.setText(String.format(Locale.getDefault(),
                    mContext.getString(R.string.slot_card_time_format), s.endHour, s.endMinute));
            Button mSelect = (Button) children.findViewById(R.id.button_select);
            mSelect.setTag(s);
            mSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new ItemClickEvent<Slot>((Slot) v.getTag()));
                }
            });
            mHead.addView(children);
        }

    }
}

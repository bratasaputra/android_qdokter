package com.medisoft_international.mediqueue.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.medisoft_international.mediqueue.AzureModels.Service;
import com.medisoft_international.mediqueue.R;
import com.medisoft_international.mediqueue.events.ItemClickEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Medisoft Development on 2017-07-03.
 */

public class ServiceViewHolder extends RecyclerView.ViewHolder {
    private Service mService;
    private TextView mName;
    private TextView mDescription;
    private Button mSelectButton;

    public ServiceViewHolder(View view) {
        super(view);
        mName = (TextView) view.findViewById(R.id.service_name);
        mDescription = (TextView) view.findViewById(R.id.service_desc);
        mSelectButton = (Button) view.findViewById(R.id.button_select);
        mSelectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new ItemClickEvent<Service>(mService));
            }
        });
    }

    public void bind(Service service) {
        this.mService = service;

        mName.setText(mService.name);
        mDescription.setText(service.description);
        if (TextUtils.isEmpty(service.description))
            mDescription.setVisibility(View.GONE);
    }
}

package com.medisoft_international.mediqueue.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medisoft_international.mediqueue.AzureModels.Specialisation;
import com.medisoft_international.mediqueue.R;

import java.util.List;

/**
 * Created by Medisoft Development on 2017-06-03.
 */

public class SpesialisationAdapter extends RecyclerView.Adapter<SpesialisationViewHolder> {
    private List<Specialisation> mSpecialisations;

    public SpesialisationAdapter(List<Specialisation> specialisations) {
        this.mSpecialisations = specialisations;
    }

    public void setDataSet(List<Specialisation> specialisations) {
        this.mSpecialisations = specialisations;
        notifyDataSetChanged();
    }

    @Override
    public SpesialisationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.specialisation_card, parent, false);
        return new SpesialisationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SpesialisationViewHolder holder, int position) {
        holder.bind(mSpecialisations.get(position));
    }

    @Override
    public int getItemCount() {
        return mSpecialisations.size();
    }
}

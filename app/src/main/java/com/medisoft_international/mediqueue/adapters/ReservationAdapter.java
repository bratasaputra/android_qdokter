package com.medisoft_international.mediqueue.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medisoft_international.mediqueue.AzureModels.Reservation;
import com.medisoft_international.mediqueue.AzureModels.ReservationDB;
import com.medisoft_international.mediqueue.R;
import java.util.List;

public class ReservationAdapter extends RecyclerView.Adapter<ReservationViewHolder> {
    private List<Reservation> mReservations;

    public ReservationAdapter(List<Reservation> reservations) {
        this.mReservations = reservations;
    }

    public void setDataSet(List<Reservation> reservations) {
        this.mReservations = reservations;
        notifyDataSetChanged();
    }


    @Override
    public ReservationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reservation_card, parent, false);
        return new ReservationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReservationViewHolder holder, int position) {
        holder.bind(mReservations.get(position));
    }

    @Override
    public int getItemCount() {
        return mReservations.size();
    }
}

package com.medisoft_international.mediqueue.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medisoft_international.mediqueue.AzureModels.Hospital;
import com.medisoft_international.mediqueue.R;

import java.util.List;

public class HospitalAdapter extends RecyclerView.Adapter<HospitalViewHolder> {
    private List<Hospital> mHospitals;
    private Context mContext;

    public HospitalAdapter(@NonNull List<Hospital> hospitals, Context context) {
        this.mHospitals = hospitals;
        this.mContext = context;
    }

    public void setDataSet(List<Hospital> hospitals) {
        this.mHospitals = hospitals;
        notifyDataSetChanged();
    }

    @Override
    public HospitalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hospital_card, parent, false);
        return new HospitalViewHolder(view, mContext);
    }

    @Override
    public void onBindViewHolder(HospitalViewHolder holder, int position) {
        holder.bind(mHospitals.get(position));
    }

    @Override
    public int getItemCount() {
        return mHospitals.size();
    }
}

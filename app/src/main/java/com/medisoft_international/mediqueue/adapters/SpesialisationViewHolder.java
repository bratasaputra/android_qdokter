package com.medisoft_international.mediqueue.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.medisoft_international.mediqueue.AzureModels.Specialisation;
import com.medisoft_international.mediqueue.R;
import com.medisoft_international.mediqueue.events.ItemClickEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Medisoft Development on 2017-06-03.
 */

public class SpesialisationViewHolder extends RecyclerView.ViewHolder {
    private Specialisation mSpesialis;
    private TextView mName;

    public SpesialisationViewHolder(View view) {
        super(view);
        mName = (TextView) view.findViewById(R.id.text);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new ItemClickEvent<Specialisation>(mSpesialis));
            }
        });
    }

    public void bind(Specialisation spesialis) {
        this.mSpesialis = spesialis;
        mName.setText(mSpesialis.name);
    }
}

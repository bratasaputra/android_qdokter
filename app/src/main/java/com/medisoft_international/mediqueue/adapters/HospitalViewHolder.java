package com.medisoft_international.mediqueue.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.medisoft_international.mediqueue.AzureModels.Hospital;
import com.medisoft_international.mediqueue.R;
import com.medisoft_international.mediqueue.events.ItemClickEvent;
import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

public class HospitalViewHolder extends RecyclerView.ViewHolder {
    private Hospital mHospital;
    private TextView mAddress;
    private TextView mDistance;
    private ImageView mLogo;
    private TextView mName;
    private Context mContext;

    public HospitalViewHolder(View view, Context context) {
        super(view);
        mContext = context;
        mAddress = (TextView) view.findViewById(R.id.hospital_name);
        mDistance = (TextView) view.findViewById(R.id.hospital_address);
        mLogo = (ImageView) view.findViewById(R.id.hospital_image);
        mName = (TextView) view.findViewById(R.id.hospital_text);
        Button mViewButton = (Button) view.findViewById(R.id.hospital_button);
        mViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new ItemClickEvent<Hospital>(mHospital));
            }
        });
    }

    public void bind(Hospital hospital) {
        this.mHospital = hospital;
        mAddress.setText(hospital.address);
        mDistance.setText(String.format(Locale.getDefault(), "%.02f KM", hospital.meta_jarak));
        if (hospital.meta_jarak == null)
            mDistance.setVisibility(View.GONE);
        else
            mDistance.setVisibility(View.VISIBLE);
        mName.setText(hospital.name);
        if (mHospital.image == null) {
            mLogo.setVisibility(View.GONE);
        } else {
            Glide.with(mContext).load(mHospital.image).fitCenter().into(mLogo);
        }
    }
}

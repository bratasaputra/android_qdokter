package com.medisoft_international.mediqueue.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medisoft_international.mediqueue.AzureModels.DisplaySlot;
import com.medisoft_international.mediqueue.R;

import java.util.List;

/**
 * Created by Medisoft Development on 2017-07-03.
 */

public class SlotAdapter2 extends RecyclerView.Adapter<SlotViewHolder2> {
    protected List<DisplaySlot> mSlots;

    public SlotAdapter2(List<DisplaySlot> slots) {
        this.mSlots = slots;
    }

    public void setDataSet(List<DisplaySlot> slots) {
        this.mSlots = slots;
        notifyDataSetChanged();
    }

    @Override
    public SlotViewHolder2 onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.slot_card_new, parent, false);
        return new SlotViewHolder2(view);
    }

    @Override
    public void onBindViewHolder(SlotViewHolder2 holder, int position) {
        holder.bind(mSlots.get(position));
    }

    @Override
    public int getItemCount() {
        return mSlots.size();
    }
}

package com.medisoft_international.mediqueue.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.medisoft_international.mediqueue.AzureModels.Service;
import com.medisoft_international.mediqueue.R;
import com.medisoft_international.mediqueue.Util;

import java.util.List;

/**
 * Created by Medisoft Development on 2017-07-03.
 */

public class ServiceAdapter extends RecyclerView.Adapter<ServiceViewHolder> {
    private List<Service> mServices;

    public ServiceAdapter(List<Service> services) {
        this.mServices = services;
    }

    public void setDataSet(List<Service> services) {
        this.mServices = services;
        notifyDataSetChanged();
    }


    @Override
    public ServiceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_card, parent, false);
        return new ServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ServiceViewHolder holder, int position) {
        Util.Log("On Bind");
        holder.bind(mServices.get(position));
    }

    @Override
    public int getItemCount() {
        return mServices.size();
    }
}

package com.medisoft_international.mediqueue.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.medisoft_international.mediqueue.AzureModels.QueueEntry;
import com.medisoft_international.mediqueue.R;
import java.util.List;

public class QueueAdapter extends RecyclerView.Adapter<QueueViewHolder> {
    private List<QueueEntry> mQueues;

    public QueueAdapter(@NonNull List<QueueEntry> queues) {
        this.mQueues = queues;
    }

    public void setDataSet(List<QueueEntry> queues) {
        this.mQueues = queues;
        notifyDataSetChanged();
    }


    @Override
    public QueueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.queue_card, parent, false);
        return new QueueViewHolder(view);
    }

    @Override
    public void onBindViewHolder(QueueViewHolder holder, int position) {
        holder.bind(mQueues.get(position));
    }

    @Override
    public int getItemCount() {
        return mQueues.size();
    }
}

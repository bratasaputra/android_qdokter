package com.medisoft_international.mediqueue.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import com.medisoft_international.mediqueue.AzureModels.Hospital;
import com.medisoft_international.mediqueue.AzureModels.Reservation;
import com.medisoft_international.mediqueue.AzureModels.ReservationDB;
import com.medisoft_international.mediqueue.AzureModels.Service;
import com.medisoft_international.mediqueue.Medisoft;
import com.medisoft_international.mediqueue.R;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ReservationViewHolder extends RecyclerView.ViewHolder {
    private Reservation mReservation;
    private TextView mBookingCode;
    private TextView mBookingTime;
    private TextView mBookingDate;
    private TextView mServiceName;
    private TextView mHospitalName;

    public ReservationViewHolder(View view) {
        super(view);
        mBookingCode = (TextView) view.findViewById(R.id.booking_code);
        mBookingDate = (TextView) view.findViewById(R.id.booking_date);
        mBookingTime = (TextView) view.findViewById(R.id.booking_time);
        mServiceName = (TextView) view.findViewById(R.id.booking_service_name);
        mHospitalName = (TextView) view.findViewById(R.id.booking_hospital_name);
        mHospitalName.setVisibility(View.VISIBLE);
        mBookingTime.setVisibility(View.VISIBLE);
    }

    public void bind(Reservation reservation) {
        this.mReservation = reservation;
        mBookingCode.setText(mReservation.bookingCode);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH : mm", Locale.getDefault());
        final Calendar calendar = Calendar.getInstance();
        final Date today = calendar.getTime();
        calendar.set(Calendar.YEAR, Integer.valueOf(mReservation.appointmentDate.substring(0, 4)));
        calendar.set(Calendar.MONTH, Integer.valueOf(mReservation.appointmentDate.substring(4, 6)) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(mReservation.appointmentDate.substring(6, 8)));
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(mReservation.appointmentTime.substring(0, 2)));
        calendar.set(Calendar.MINUTE, Integer.valueOf(mReservation.appointmentTime.substring(2, 4)));
        calendar.set(Calendar.SECOND, 0);
        mBookingDate.setText(dateFormat.format(calendar.getTime()));
        mBookingTime.setText(timeFormat.format(calendar.getTime()));
        if (dateFormat.format(today).equals(dateFormat.format(calendar.getTime()))) {
            mBookingDate.setTextColor(Color.RED);
            mBookingTime.setTextColor(Color.RED);
        } else {
            mBookingDate.setTextColor(ContextCompat.getColor(mBookingDate.getContext(), R.color.primary_text));
            mBookingTime.setTextColor(ContextCompat.getColor(mBookingTime.getContext(), R.color.primary_text));
        }
        if (TextUtils.isEmpty(mReservation.serviceName)) {
        } else {
            mServiceName.setText(mReservation.serviceName);
        }
        if (!TextUtils.isEmpty(mReservation.hospitalName)) {
            mHospitalName.setText(mReservation.hospitalName);
        } else {
        }
    }
}

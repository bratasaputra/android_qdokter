package com.medisoft_international.mediqueue.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.medisoft_international.mediqueue.AzureModels.Hospital;
import com.medisoft_international.mediqueue.AzureModels.QueueEntry;
import com.medisoft_international.mediqueue.Medisoft;
import com.medisoft_international.mediqueue.R;
import com.medisoft_international.mediqueue.events.DeleteItemEvent;
import com.medisoft_international.mediqueue.events.ItemClickEvent;
import org.greenrobot.eventbus.EventBus;

public class QueueViewHolder extends RecyclerView.ViewHolder {
    private QueueEntry mQueue;
    private TextView mNumber;
    private TextView mName;
    private TextView mService;

    public QueueViewHolder(View view) {
        super(view);
        mNumber = (TextView) view.findViewById(R.id.q_number);
        mService = (TextView) view.findViewById(R.id.service_name);
        mName = (TextView) view.findViewById(R.id.hospital_name);
        Button mCheckButton = (Button) view.findViewById(R.id.check_queue_button);
        mCheckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new ItemClickEvent<>(mQueue));
            }
        });
        ImageButton mDeleteButton = (ImageButton) view.findViewById(R.id.delete_queue);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new DeleteItemEvent<>(mQueue));
            }
        });
    }

    public void bind(QueueEntry queue) {
        this.mQueue = queue;
        mNumber.setText(mQueue.qNumber);
        mService.setText(mQueue.serviceName);
    }
}

package com.medisoft_international.mediqueue.events;

/**
 * Created by Medisoft Development on 2017-07-03.
 */

public class DateTimeSetEvent<T> {
    public final T item;

    public DateTimeSetEvent(T item) {
        this.item = item;
    }
}

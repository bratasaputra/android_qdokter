package com.medisoft_international.mediqueue.events;

/**
 * Created by Medisoft Development on 2017-06-03.
 */

public class ItemClickEvent<T> {
    public final T item;

    public ItemClickEvent(T item) {
        this.item = item;
    }
}

package com.medisoft_international.mediqueue.events;

import com.google.android.gms.vision.barcode.Barcode;

/**
 * Created by Medisoft Development on 2017-28-02.
 */

public class BarcodeDetectedEvent {
    public final Barcode barcode;

    public BarcodeDetectedEvent(Barcode barcode) {
        this.barcode = barcode;
    }
}

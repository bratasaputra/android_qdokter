package com.medisoft_international.mediqueue.events;

/**
 * Created by Medisoft Development on 2017-07-03.
 */

public class OpenItemEvent<T> {
    public final T item;

    public OpenItemEvent(T item) {
        this.item = item;
    }
}

package com.medisoft_international.mediqueue.events;

/**
 * Created by Medisoft Development on 2017-20-03.
 */

public class DeleteItemEvent<T> {
    public T item;

    public DeleteItemEvent(T item) {
        this.item = item;
    }
}

package com.medisoft_international.mediqueue.events;

import com.medisoft_international.mediqueue.AzureModels.Hospital;
import com.medisoft_international.mediqueue.AzureModels.Reservation;
import com.medisoft_international.mediqueue.AzureModels.Service;
import com.medisoft_international.mediqueue.AzureModels.Slot;

/**
 * Created by Medisoft Development on 2017-15-03.
 */

public class StickyReservationData {
    public Hospital hospital;
    public Service service;
    public Slot slot;
    public Reservation reservation;

    public StickyReservationData() {

    }
}

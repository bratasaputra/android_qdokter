package com.medisoft_international.mediqueue;

import android.content.Context;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class Medisoft {
    private static boolean hasInit = false;
    private static RequestQueue mRequestQueue;

    static void init(Context context) {
        if (!hasInit) {
            Realm.init(context.getApplicationContext());
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .deleteRealmIfMigrationNeeded()
                    .build();
            Realm.setDefaultConfiguration(config);

            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
            mRequestQueue.start();

            hasInit = true;
        }
    }

    static RequestQueue getDefaultRequestQueue() {
        return mRequestQueue;
    }
}

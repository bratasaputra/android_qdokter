package com.medisoft_international.mediqueue;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureModels.Reservation;
import com.medisoft_international.mediqueue.AzureModels.ReservationDB;
import com.medisoft_international.mediqueue.AzureModels.Service;
import com.medisoft_international.mediqueue.VolleyRequest.GetServiceRequest;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import io.realm.Realm;

public class BookingConfirmationActivity extends AppCompatActivity
        implements View.OnClickListener {
    protected TextView bookingCode;
    protected TextView appointmentDate;
    protected TextView serviceName;
    protected Button doneButton;
    protected Reservation mReservation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_confirmation);
        bookingCode = (TextView) findViewById(R.id.booking_code);
        appointmentDate = (TextView) findViewById(R.id.booking_date);
        serviceName = (TextView) findViewById(R.id.booking_service_name);
        serviceName.setText("");
        doneButton = (Button) findViewById(R.id.booking_done_button);
        doneButton.setEnabled(false);
        doneButton.setOnClickListener(this);
        mReservation = new Gson().fromJson(getIntent().getStringExtra(Reservation.class.getSimpleName()), Reservation.class);
        if (mReservation == null) {
            finish();
        } else {
            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(new ReservationDB().create(mReservation));
            realm.commitTransaction();
            bookingCode.setText(mReservation.bookingCode);
            SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, Integer.valueOf(mReservation.appointmentDate.substring(0, 4)));
            calendar.set(Calendar.MONTH, Integer.valueOf(mReservation.appointmentDate.substring(4, 6)) - 1);
            calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(mReservation.appointmentDate.substring(6, 8)));
            appointmentDate.setText(format.format(calendar.getTime()));
            GetServiceRequest request = new GetServiceRequest(mReservation.service,
                    new Response.Listener<Service>() {
                        @Override
                        public void onResponse(Service response) {
                            serviceName.setText(response.name);
                            doneButton.setEnabled(true);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Snackbar.make(findViewById(R.id.confirmation_card), R.string.unable_to_connect_to_server, Snackbar.LENGTH_LONG).show();
                    doneButton.setEnabled(true);
                }
            });
            Medisoft.getDefaultRequestQueue().add(request);
        }
    }

    @Override
    public void onClick(View v) {
        Intent clearTopIntent = new Intent(this, MainActivity.class);
        clearTopIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(clearTopIntent);
    }

    @Override
    public void onBackPressed() {
        Intent clearTopIntent = new Intent(this, MainActivity.class);
        clearTopIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(clearTopIntent);
    }
}

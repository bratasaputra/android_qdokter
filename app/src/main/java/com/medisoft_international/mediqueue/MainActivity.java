package com.medisoft_international.mediqueue;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresPermission;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureModels.Hospital;
import com.medisoft_international.mediqueue.AzureModels.Patient;
import com.medisoft_international.mediqueue.AzureModels.Specialisation;
import com.medisoft_international.mediqueue.VolleyRequest.GetSpesialisListRequest;
import com.medisoft_international.mediqueue.VolleyRequest.SearchHospitalRequest;
import com.medisoft_international.mediqueue.adapters.HospitalAdapter;
import com.medisoft_international.mediqueue.events.ItemClickEvent;
import com.medisoft_international.mediqueue.events.StickyReservationData;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements AdapterView.OnItemSelectedListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static int REQUEST_CHECK_SETTINGS = 9999;
    protected Toolbar mToolbar;
    protected Spinner mSpinner;
    protected ArrayAdapter<Specialisation> mAdapter;
    protected EditText mSearch;
    protected ImageButton mSearchButton;
    protected GoogleApiClient mGoogleApiClient;
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected HospitalAdapter mHospitalAdapter;
    protected List<Specialisation> mSpecialisations;
    protected String searchTerm = null;
    protected String spesialisTerm = null;
    protected Location mLocation = null;
    protected Double longitude = null;
    protected Double latitude = null;
    protected double defaultMax = 30000;
    protected boolean inProgress = false;
    protected ProgressBar progressBar;
    protected Button checkReservation;
    protected Button checkQueue;
    ImageButton profileButton;
//    protected Patient mPatient;
    protected int spesialisTryCount = 0;
    protected int hospitalTryCount = 0;
    protected boolean hasFinishInit = false;
    protected LocationRequest mLocationRequest;
    FloatingActionButton mRefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_bottombar);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        mSpinner = (Spinner) findViewById(R.id.search_option_spinner);
        mSpinner.setOnItemSelectedListener(this);
        mSearch = (EditText) findViewById(R.id.search_term);
        mSearch.setHint(R.string.search_hospital_hint);
        mSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchHospital();
                    return true;
                }
                return false;
            }
        });
        mSearchButton = (ImageButton) findViewById(R.id.start_search_button);
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchHospital();
            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mRecyclerView = (RecyclerView) findViewById(R.id.search_result);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mHospitalAdapter = new HospitalAdapter(new ArrayList<Hospital>(), this);
        mRecyclerView.setAdapter(mHospitalAdapter);
        checkQueue = (Button) findViewById(R.id.check_queue_button);
        checkQueue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), QueueListActivity.class));
            }
        });
        checkReservation = (Button) findViewById(R.id.check_reservation_button);
        checkReservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ResevationListActivity.class));
            }
        });
        profileButton = (ImageButton) findViewById(R.id.profile_button);
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent goToProfile = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(goToProfile);
            }
        });
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setInterval(5000);
        progress(false);
        mSearchButton.setEnabled(hasFinishInit);
        mSpinner.setEnabled(hasFinishInit);
        mRefresh = (FloatingActionButton) findViewById(R.id.refreshButton);
        mRefresh.setVisibility(View.GONE);
        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchHospital();
            }
        });
    }

    private void searchHospital() {
        getLocation();
        if (inProgress) {
            inProgress = false;
        }
        progress(true);
        mSearch.clearFocus();
        if (TextUtils.isEmpty(mSearch.getText())) {
            searchTerm = null;
        } else {
            searchTerm = mSearch.getText().toString();
        }

        Medisoft.getDefaultRequestQueue().cancelAll(SearchHospitalRequest.class);
        Map<String, String> searchParams = new HashMap<>();
        if (spesialisTerm != null) {
            searchParams.put("spesialis", spesialisTerm);
        }
        if (searchTerm != null) {
            searchParams.put("term", searchTerm);
        }
        if (mLocation != null) {
            searchParams.put("longitude", longitude == null ? null : longitude.toString());
            searchParams.put("latitude", latitude == null ? null : latitude.toString());
            searchParams.put("maxDistance", mLocation == null ? null : String.valueOf(defaultMax));
        }
        SearchHospitalRequest request = new SearchHospitalRequest(searchParams, new Response.Listener<List<Hospital>>() {
            @Override
            public void onResponse(List<Hospital> response) {
                mHospitalAdapter.setDataSet(response);
                progress(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Util.Log(error);
                Snackbar.make(mRecyclerView, R.string.unable_to_search,
                        Snackbar.LENGTH_LONG).show();
                progress(false);
                mRefresh.setVisibility(View.VISIBLE);
            }
        });
        mRefresh.setVisibility(View.GONE);
        Medisoft.getDefaultRequestQueue().add(request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.check_queue:
                startActivity(new Intent(this, QueueListActivity.class));
                return true;
            case R.id.check_reservation:
                startActivity(new Intent(this, ResevationListActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GetSpesialisListRequest spesialisListRequest = new GetSpesialisListRequest(new Response.Listener<List<Specialisation>>() {
            @Override
            public void onResponse(List<Specialisation> response) {
                mSpecialisations = response;
                Specialisation nullEntry = new Specialisation();
                nullEntry.name = getString(R.string.choose_spesialis);
                mSpecialisations.add(0, nullEntry);
                mAdapter = new ArrayAdapter<>(getApplicationContext(),
                        R.layout.specialisation_card, mSpecialisations);
                mAdapter.setDropDownViewResource(R.layout.specialisation_card);
                mSpinner.setAdapter(mAdapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress(false);
            }
        });
        Medisoft.getDefaultRequestQueue().add(spesialisListRequest);
        mGoogleApiClient.connect();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().removeStickyEvent(StickyReservationData.class);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Util.Log("Item Selected");
        if (position == 0)
            spesialisTerm = null;
        else {
            spesialisTerm = mSpecialisations.get(position).name;
            if (!mSpecialisations.get(0).name.equals("Others")) {
                mSpecialisations.get(0).name = "Others";
                mAdapter = new ArrayAdapter<>(getApplicationContext(),
                        R.layout.specialisation_card, mSpecialisations);
                mAdapter.setDropDownViewResource(R.layout.specialisation_card);
                mSpinner.setAdapter(mAdapter);
                mSpinner.setSelection(position);
                return;
            }
        }

        if (hasFinishInit) {
            Util.Log("Inside spinner");
            if (inProgress) {
            }
            progress(true);
            searchHospital();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (checkPermission()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            if (!hasFinishInit)
                checkGpsStatus();
            Util.Log(mLocation);
        } else {
            requestPermission();
        }
    }

    public boolean checkPermission() {
        return ContextCompat
                .checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        if (!ActivityCompat
                .shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }
    }

    @Override
    @RequiresPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case 0:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkPermission()) {
                        checkGpsStatus();
                    }
                } else {
                    mLocation = null;
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    public void checkGpsStatus() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY))
                .addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY));
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        if (!hasFinishInit) {
                            progress(true);
                            finishInit();
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    MainActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (!hasFinishInit) {
                progress(true);
                finishInit();
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressWarnings({"MissingPermission"})
    public void getLocation() {
        if (mLocation != null) {
            longitude = mLocation.getLongitude();
            latitude = mLocation.getLatitude();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mLocation = null;
    }

    @Subscribe
    public void onItemClickEvent(ItemClickEvent<Hospital> event) {
        Intent intent = new Intent(this, HospitalListActivity.class);
        intent.putExtra(Hospital.class.getSimpleName(), new Gson().toJson(event.item));
        startActivity(intent);
    }

    public void progress(boolean disable) {
        inProgress = disable;
        if (inProgress) {
            progressBar.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mLocation == null) {
            Util.Log(location);
            mLocation = location;
            searchHospital();
        } else {
            mLocation = location;
        }
    }

    public void finishInit() {
        if (!hasFinishInit) {
            hasFinishInit = true;
            mSearchButton.setEnabled(hasFinishInit);
            mSpinner.setEnabled(hasFinishInit);
            searchHospital();
        }
    }
}

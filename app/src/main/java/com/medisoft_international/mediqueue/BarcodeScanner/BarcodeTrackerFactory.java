package com.medisoft_international.mediqueue.BarcodeScanner;

import android.content.Context;

import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.barcode.Barcode;

/**
 * Created by Medisoft Development on 2017-28-02.
 */

public class BarcodeTrackerFactory implements MultiProcessor.Factory<Barcode> {
    private GraphicOverlay<BarcodeGraphic> mGraphicOverlay;
    private Context mContext;

    public BarcodeTrackerFactory(GraphicOverlay<BarcodeGraphic> barcodeGraphicOverlay, Context context) {
        mGraphicOverlay = barcodeGraphicOverlay;
        mContext = context;
    }

    @Override
    public Tracker<Barcode> create(Barcode barcode) {
        BarcodeGraphic graphic = new BarcodeGraphic(mGraphicOverlay, mContext);
        return new BarcodeGraphicTracker(mGraphicOverlay, graphic);
    }
}


package com.medisoft_international.mediqueue;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureModels.Patient;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class LandingActivity extends AppCompatActivity implements View.OnClickListener {
    protected Button loginButton;
    protected Button signupButton;
    protected Patient mPatient = null;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        loginButton = (Button) findViewById(R.id.log_in_button);
        loginButton.setOnClickListener(this);
        signupButton = (Button) findViewById(R.id.sign_up_button);
        signupButton.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!checkPermission()) {
            requestPermission();
        }
    }

    public boolean checkPermission() {
        return ContextCompat
                .checkSelfPermission(this,
                        Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    public void requestPermission() {
        if (!ActivityCompat
                .shouldShowRequestPermissionRationale(this,
                        Manifest.permission.RECEIVE_SMS)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECEIVE_SMS}, 0);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.log_in_button:
                Intent openLoginActivity = new Intent(this, LoginActivity.class);
                startActivity(openLoginActivity);
                break;
            case R.id.sign_up_button:
                Intent openSignupActivity = new Intent(this, SignUpActivity.class);
                startActivity(openSignupActivity);
                break;
        }
    }
}

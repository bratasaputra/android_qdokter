package com.medisoft_international.mediqueue;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureModels.Hospital;
import com.medisoft_international.mediqueue.events.StickyReservationData;

import org.greenrobot.eventbus.EventBus;

public class HospitalListActivity extends AppCompatActivity implements OnMapReadyCallback {
    protected Hospital mHospital;
    protected Button mBookButton;
    protected TextView mTitle;
    protected MapView mMapView;
    protected GoogleMap mGoogleMap;
    protected GoogleMapOptions mOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_list);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mHospital = new Gson().fromJson(getIntent()
                .getStringExtra(Hospital.class.getSimpleName()), Hospital.class);
        Util.Log(mHospital);

        if (mHospital == null) {
            finish();
        } else {
            StickyReservationData data = EventBus.getDefault().removeStickyEvent(StickyReservationData.class);
            if (data == null) {
                data = new StickyReservationData();
                data.hospital = mHospital;
                EventBus.getDefault().postSticky(data);
            }
            mTitle = (TextView) findViewById(R.id.hospital_title);
            mTitle.setText(mHospital.name);
            getSupportActionBar().setTitle("Detil Rumah Sakit");
            mBookButton = (Button) findViewById(R.id.book_button);
            mBookButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ServiceListActivity.class);
                    intent.putExtra(Hospital.class.getSimpleName(), new Gson().toJson(mHospital));
                    startActivity(intent);
                }
            });
            mOptions = new GoogleMapOptions();
            mMapView = (MapView) findViewById(R.id.hospital_map);
            mMapView.onCreate(null);
            mMapView.getMapAsync(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (mGoogleMap != null) {
            LatLng position = new LatLng(mHospital.latitude, mHospital.longitude);
            mGoogleMap.addMarker(new MarkerOptions().position(position));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 18f));
        }
    }
}

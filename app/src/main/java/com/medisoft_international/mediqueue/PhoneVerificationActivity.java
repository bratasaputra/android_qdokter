package com.medisoft_international.mediqueue;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureModels.Patient;
import com.medisoft_international.mediqueue.R;
import com.medisoft_international.mediqueue.VolleyRequest.PostNewPatientRequest;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

public class PhoneVerificationActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar mToolbar;
    Patient mPatient;
    EditText mCode;
    Button mVerifyButton;
    Button mResendCode;
    String mVerificationId;
    FirebaseAuth mAuth;
    ProgressBar mProgress;

    PhoneAuthProvider.ForceResendingToken mResendToken;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            signInWithPhoneAuthCredential(phoneAuthCredential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(PhoneVerificationActivity.this);
            alertBuilder.setCancelable(false);
            if (e instanceof FirebaseAuthInvalidCredentialsException) {
                alertBuilder.setTitle("Invalid phone number");
                alertBuilder.setMessage("Please insert a valid phone number");
                alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
            } else if (e instanceof FirebaseTooManyRequestsException) {
                alertBuilder.setTitle("Server error");
                alertBuilder.setMessage("Authentication limit has been reached, please contact Qdokter support page");
                alertBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
            }
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            mVerificationId = s;
            mResendToken = forceResendingToken;
        }

        @Override
        public void onCodeAutoRetrievalTimeOut(String s) {
            super.onCodeAutoRetrievalTimeOut(s);
            mResendCode.setEnabled(true);
        }
    };

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = task.getResult().getUser();
                            // Check if user has been registered
                            if (mPatient._id != null) {
                                Util.savePatient(PhoneVerificationActivity.this, mPatient);
                                Util.StickPatient(mPatient);
                                Intent goToMain = new Intent(PhoneVerificationActivity.this, MainActivity.class);
                                goToMain.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(goToMain);
                            } else {
                                // Complete user registration here, then go to main activity
                                PostNewPatientRequest request = new PostNewPatientRequest(mPatient, new Response.Listener<Patient>() {
                                    @Override
                                    public void onResponse(Patient response) {
                                        //Registration succesfull
                                        //Save to shared pref
                                        //Go to main
                                        Util.savePatient(PhoneVerificationActivity.this, response);
                                        Util.StickPatient(response);
                                        Intent goToMain = new Intent(PhoneVerificationActivity.this, MainActivity.class);
                                        goToMain.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(goToMain);
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        //Show error message
                                        Util.Log(error);
                                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(PhoneVerificationActivity.this);
                                        alertDialog.setTitle("Unable To Register");
                                        alertDialog.setMessage("Please try again later");
                                        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                Intent goToLanding = new Intent(PhoneVerificationActivity.this, LandingActivity.class);
                                                goToLanding.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(goToLanding);
                                            }
                                        });
                                    }
                                });
                                Medisoft.getDefaultRequestQueue().add(request);
                            }
                        } else {
                            mVerifyButton.setVisibility(View.VISIBLE);
                            mProgress.setVisibility(View.GONE);
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(PhoneVerificationActivity.this);
                                alertDialog.setTitle("Invalid Code");
                                alertDialog.setMessage("Please enter the 6-digits code send to your phone");
                                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                alertDialog.setNegativeButton("Resend Code", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        sendVerificationCode();
                                        dialog.dismiss();
                                    }
                                });
                            }
                        }
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mAuth = FirebaseAuth.getInstance();
        mCode = (EditText) findViewById(R.id.verificationCode);
        mVerifyButton = (Button) findViewById(R.id.verifyButton);
        mVerifyButton.setOnClickListener(this);
        mResendCode = (Button) findViewById(R.id.resendCode);
        mResendCode.setEnabled(false);
        mResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mResendCode.setEnabled(false);
                sendVerificationCode();
            }
        });
        mProgress = (ProgressBar) findViewById(R.id.progressBar);
        mProgress.setVisibility(View.GONE);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Verify your phone number");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mPatient = Util.GetPatient(this);
        if (mPatient != null) {
            sendVerificationCode();
        }
        else {
            // No Patient is passed here, something is wrong, finish this activity
            finish();
        }
    }

    private void sendVerificationCode() {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(mPatient.phone,
                60, TimeUnit.SECONDS, this, mCallback);
        mCode.setText(null);
        mCode.requestFocus();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.verifyButton:
                String code = mCode.getText().toString();
                if (!TextUtils.isEmpty(mCode.getText()) && TextUtils.isDigitsOnly(mCode.getText())) {
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
                    mVerifyButton.setVisibility(View.GONE);
                    mProgress.setVisibility(View.VISIBLE);
                    signInWithPhoneAuthCredential(credential);
                }
                break;
        }
    }
}

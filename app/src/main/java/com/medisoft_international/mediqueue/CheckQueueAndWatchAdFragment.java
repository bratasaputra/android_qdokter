package com.medisoft_international.mediqueue;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveVideoTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.medisoft_international.mediqueue.AzureModels.Ads;
import com.medisoft_international.mediqueue.AzureModels.Queue;
import com.medisoft_international.mediqueue.AzureModels.QueueEntry;
import com.medisoft_international.mediqueue.VolleyRequest.GetAdsRequest;
import com.medisoft_international.mediqueue.VolleyRequest.TrackQueueRequest;
import com.medisoft_international.mediqueue.events.DeleteItemEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;

public class CheckQueueAndWatchAdFragment extends DialogFragment {
    public int secondCount = 0;
    private QueueEntry mQueue;
    private SimpleExoPlayer player;
    private Button skipButton;
    private Handler handler = new Handler();
    private Runnable run = new Runnable() {
        @Override
        public void run() {
            skipButton.setVisibility(View.VISIBLE);
            ++secondCount;
        }
    };
    private CardView countCard;
    private TextView countText;
    private SimpleExoPlayerView simpleExoPlayerView;
    private ProgressBar progressBar;
    private View morethan5;
    private View lessthan5;

    public CheckQueueAndWatchAdFragment() {
        // Required empty public constructor
    }

    public static CheckQueueAndWatchAdFragment Create(QueueEntry queue) {
        CheckQueueAndWatchAdFragment fragment = new CheckQueueAndWatchAdFragment();
        fragment.mQueue = queue;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_check_queue, container, false);
        lessthan5 = view.findViewById(R.id.less_than_five);
        lessthan5.setVisibility(View.GONE);
        morethan5 = view.findViewById(R.id.more_than_five);
        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        skipButton = (Button) view.findViewById(R.id.video_skip);
        skipButton.setVisibility(View.GONE);
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (player != null) {
                    player.stop();
                    player.release();
                }
                skipButton.setVisibility(View.GONE);
                simpleExoPlayerView.setVisibility(View.GONE);
                countCard.setVisibility(View.VISIBLE);
            }
        });

        countCard = (CardView) view.findViewById(R.id.count_card);
        countCard.setVisibility(View.GONE);
        countText = (TextView) view.findViewById(R.id.waiting_count);
        countText.setText("??");

        simpleExoPlayerView = (SimpleExoPlayerView) view.findViewById(R.id.video_frame);
        simpleExoPlayerView.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LoadControl loadControl = new DefaultLoadControl();

        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveVideoTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        player = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector, loadControl);
        simpleExoPlayerView.setPlayer(player);

        if (mQueue == null) {
            dismiss();
        } else {
            String id = mQueue._id;
            //Get Ads
//            GetAdsRequest adsRequest = new GetAdsRequest(new Response.Listener<Ads>() {
//                @Override
//                public void onResponse(Ads response) {
//                    prepareVideo(response);
//                }
//            }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    skipButton.setVisibility(View.GONE);
//                    progressBar.setVisibility(View.GONE);
//                    simpleExoPlayerView.setVisibility(View.GONE);
//                    countCard.setVisibility(View.VISIBLE);
//                }
//            });
            //Get Queue Count
            TrackQueueRequest trackQueue = new TrackQueueRequest(id,
                    new Response.Listener<Queue.QueueCount>() {
                        @Override
                        public void onResponse(Queue.QueueCount response) {
                            progressBar.setVisibility(View.GONE);
                            if (response == null) {
                                EventBus.getDefault().post(new DeleteItemEvent<>(mQueue));
                                Toast.makeText(getContext(), "Nomor sudah lewat", Toast.LENGTH_SHORT).show();
                                dismiss();
                            } else {
                                countCard.setVisibility(View.VISIBLE);
                                if (response.Count > 5) {
                                    countText.setText(String.format(Locale.getDefault(),
                                            getString(R.string.format_count_waiting), response.Count));
                                } else {
                                    lessthan5.setVisibility(View.VISIBLE);
                                    morethan5.setVisibility(View.GONE);
                                }
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Snackbar.make(countCard, R.string.unable_to_retrieve_data, Snackbar.LENGTH_LONG).show();
                }
            });
            Medisoft.getDefaultRequestQueue().add(trackQueue);
//            Medisoft.getDefaultRequestQueue().add(adsRequest);
        }
    }

    public void prepareVideo(Ads ad) {
        if (ad == null || ad.adsUrl == null) dismiss();
        else {
            DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getActivity(),
                    com.google.android.exoplayer2.util.Util.getUserAgent(getActivity(), "yourApplicationName"));
            ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
            MediaSource mediaSource = new ExtractorMediaSource(
                    Uri.parse(ad.adsUrl),
                    dataSourceFactory, extractorsFactory, null, null);
            player.prepare(mediaSource);
            player.setPlayWhenReady(true);
            player.addListener(new ExoPlayer.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest) {
                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                }

                @Override
                public void onLoadingChanged(boolean isLoading) {

                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    if (playbackState == ExoPlayer.STATE_READY) {
                        simpleExoPlayerView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        handler.postDelayed(run, 10000);

                    } else if (playbackState == ExoPlayer.STATE_BUFFERING) {
                        progressBar.setVisibility(View.VISIBLE);
                    } else if (playbackState == ExoPlayer.STATE_ENDED) {
                        skipButton.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        simpleExoPlayerView.setVisibility(View.GONE);
                        countCard.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {
                    skipButton.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    simpleExoPlayerView.setVisibility(View.GONE);
                    countCard.setVisibility(View.VISIBLE);
                }

                @Override
                public void onPositionDiscontinuity() {

                }
            });
        }
    }


    @Override
    public void onStop() {
        if (player != null) {
            if (player.getPlaybackState() != ExoPlayer.STATE_READY && player.getPlaybackState() != ExoPlayer.STATE_BUFFERING)
                player.release();
        }
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (player != null) {
            player.setPlayWhenReady(false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (player != null) {
            player.setPlayWhenReady(true);
        }
    }
}

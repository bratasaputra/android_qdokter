package com.medisoft_international.mediqueue;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.AsYouTypeFormatter;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.medisoft_international.mediqueue.AzureModels.Patient;
import com.medisoft_international.mediqueue.AzureModels.Reservation;
import com.medisoft_international.mediqueue.AzureModels.ReservationDB;
import com.medisoft_international.mediqueue.VolleyRequest.GetReservationRequest;
import com.medisoft_international.mediqueue.adapters.ReservationAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

public class ResevationListActivity extends AppCompatActivity {
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected ReservationAdapter mReservationAdapter;
    protected Patient mPatient;
    protected Toolbar mToolbar;
    protected Realm realm;
    protected ProgressBar mProgressBar;
    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
    private String today = format.format(calendar.getTime());
    FloatingActionButton mRefresh;
    FirebaseAuth mAuth;
    String phoneNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resevation_list);
        mAuth = FirebaseAuth.getInstance();
        phoneNumber = mAuth.getCurrentUser().getPhoneNumber();
        realm = Realm.getDefaultInstance();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Reservasi");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mRefresh = (FloatingActionButton) findViewById(R.id.refreshButton);
        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchReservation();
            }
        });
        mRefresh.setVisibility(View.GONE);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar2);
        mRecyclerView = (RecyclerView) findViewById(R.id.reservation_list);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mReservationAdapter = new ReservationAdapter(new ArrayList<Reservation>());
        mRecyclerView.setAdapter(mReservationAdapter);
        mProgressBar.setVisibility(View.GONE);
    }

    private void populateListView() {
        RealmResults<Reservation> list = realm.where(Reservation.class).findAllSorted("appointmentDate");
        List<Reservation> actualList = new ArrayList<>();
        for (Reservation x : list) {
            if (x.appointmentDate.compareTo(today) >= 0) {
                actualList.add(x);
            }
        }
        mReservationAdapter.setDataSet(actualList);
        if (actualList.isEmpty()) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        fetchReservation();
    }

    private void fetchReservation() {
        Map<String, String> searchParams = new HashMap<>();
        searchParams.put("id", phoneNumber);
        searchParams.put("retrieved", "false");
        GetReservationRequest request = new GetReservationRequest(searchParams, new Response.Listener<List<Reservation>>() {
            @Override
            public void onResponse(List<Reservation> response) {
                mProgressBar.setVisibility(View.VISIBLE);
                realm.beginTransaction();
                realm.delete(Reservation.class);
                realm.copyToRealmOrUpdate(response);
                realm.commitTransaction();
                populateListView();
                mReservationAdapter.setDataSet(response);
                mProgressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Util.Log(error);
                mRefresh.setVisibility(View.VISIBLE);
            }
        });
        mProgressBar.setVisibility(View.VISIBLE);
        mRefresh.setVisibility(View.GONE);
        Medisoft.getDefaultRequestQueue().add(request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == android.R.id.home) {
//            Intent intent = new Intent(this, MainActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intent);
//            return true;
//        }
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

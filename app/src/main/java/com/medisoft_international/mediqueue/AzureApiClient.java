package com.medisoft_international.mediqueue;

import com.android.volley.toolbox.JsonObjectRequest;
import com.medisoft_international.mediqueue.AzureModels.Ads;
import com.medisoft_international.mediqueue.AzureModels.Hospital;
import com.medisoft_international.mediqueue.AzureModels.Patient;
import com.medisoft_international.mediqueue.AzureModels.Queue;
import com.medisoft_international.mediqueue.AzureModels.QueueEntry;
import com.medisoft_international.mediqueue.AzureModels.Reservation;
import com.medisoft_international.mediqueue.AzureModels.Service;
import com.medisoft_international.mediqueue.AzureModels.Slot;
import com.medisoft_international.mediqueue.AzureModels.Specialisation;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
public class AzureApiClient {
    private static String AZURE_ENDPOINT_RELEASE = "https://medisoftonlinebooking.azurewebsites.net/";
    private static String AZURE_ENDPOINT_DEBUG = "http://192.168.2.229:64052/";

    public static final int DEFAULT_TIMEOUT_MS = 5000;
    public static final int DEFAULT_MAX_RETRIES = 3;
    public static final float DEFAULT_BACKOFF_MULT = 1.5f;

    public static final String PROTOCOL_CHARSET = "utf-8";

    /** Content type for request. */
    public static final String JSON_CONTENT_TYPE =
            String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    public static final String SERVER_URL = BuildConfig.DEBUG ? AZURE_ENDPOINT_DEBUG : AZURE_ENDPOINT_RELEASE;

    public static String searchPatientByPhoneURL(String phoneNumber) {
        String encodedPhone = null;
        try {
            encodedPhone = URLEncoder.encode(phoneNumber, "utf-8");

        } catch (UnsupportedEncodingException ex) {
        }
        if (BuildConfig.DEBUG) {
            return AZURE_ENDPOINT_DEBUG + "api/patient/phone/" + encodedPhone;
        } else {
            return AZURE_ENDPOINT_RELEASE + "api/patient/phone/" + encodedPhone;
        }
    }
}

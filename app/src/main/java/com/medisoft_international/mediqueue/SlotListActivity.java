package com.medisoft_international.mediqueue;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureModels.DisplaySlot;
import com.medisoft_international.mediqueue.AzureModels.Reservation;
import com.medisoft_international.mediqueue.AzureModels.Service;
import com.medisoft_international.mediqueue.AzureModels.Slot;
import com.medisoft_international.mediqueue.VolleyRequest.SlotListRequest;
import com.medisoft_international.mediqueue.adapters.SlotAdapter2;
import com.medisoft_international.mediqueue.events.DateTimeSetEvent;
import com.medisoft_international.mediqueue.events.ItemClickEvent;
import com.medisoft_international.mediqueue.events.StickyReservationData;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class SlotListActivity extends AppCompatActivity {
    protected Service mService;
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected SlotAdapter2 mSlotAdapter;
    protected ContentLoadingProgressBar mProgressBar;
    protected TextView mServiceName;
    FloatingActionButton mRefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        Medisoft.init(this);
        setContentView(R.layout.activity_slotlist);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mRefresh = (FloatingActionButton) findViewById(R.id.refreshButton);
        mRefresh.setVisibility(View.GONE);
        mRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetch();
            }
        });
        mService = new Gson().fromJson(getIntent().getStringExtra(Service.class.getSimpleName()), Service.class);
        if (mService == null) {
            finish();
        } else {
            StickyReservationData data = EventBus.getDefault().removeStickyEvent(StickyReservationData.class);
            if (data != null) {
                data.service = mService;
                EventBus.getDefault().postSticky(data);
            }
            mServiceName = (TextView) findViewById(R.id.service_name);
            mServiceName.setText(mService.name);
            mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
            mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mSlotAdapter = new SlotAdapter2(new ArrayList<DisplaySlot>());
            mRecyclerView.setAdapter(mSlotAdapter);
            mProgressBar = (ContentLoadingProgressBar) findViewById(R.id.slot_progress);
            fetch();
        }
    }

    private void hideLoading() {
        mProgressBar.hide();
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    private void fetch() {
        mProgressBar.show();
        mRefresh.setVisibility(View.GONE);
        Medisoft.getDefaultRequestQueue().cancelAll(SlotListRequest.class);
        SlotListRequest request = new SlotListRequest(mService._id, new Response.Listener<List<Slot>>() {
            @Override
            public void onResponse(List<Slot> response) {
                List<DisplaySlot> displaySlot = new ArrayList<>();
                if (response != null) {
                    for (Slot s : response) {
                        boolean found = false;
                        int i = 0;
                        for (DisplaySlot ds :
                                displaySlot) {
                            if (ds.dayOfWeek == s.day) {
                                found = true;
                                break;
                            }
                            i++;
                        }
                        if (!found) {
                            DisplaySlot ds = new DisplaySlot();
                            ds.dayOfWeek = s.day;
                            ds.slotList.add(s);
                            displaySlot.add(ds);
                        } else {
                            displaySlot.get(i).slotList.add(s);
                        }
                    }
                    mSlotAdapter.setDataSet(displaySlot);
                }
                hideLoading();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.hide();
                mRefresh.setVisibility(View.VISIBLE);
                Snackbar.make(mRecyclerView, R.string.unable_to_load_slot_list, Snackbar.LENGTH_LONG).show();
            }
        });
        Medisoft.getDefaultRequestQueue().add(request);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void OnItemClick(ItemClickEvent<Slot> event) {
        StickyReservationData data = EventBus.getDefault().removeStickyEvent(StickyReservationData.class);
        if (data != null) {
            data.slot = event.item;
            EventBus.getDefault().postSticky(data);
        }
        DatePickerFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.setSlot(event.item);
        datePickerFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Subscribe
    public void OnDateTimeSet(DateTimeSetEvent<Reservation> event) {
        Reservation reservation = event.item;
        reservation.service = mService._id;
        reservation.spesialisation = mService.spesialisation;

        StickyReservationData data = EventBus.getDefault().removeStickyEvent(StickyReservationData.class);
        if (data != null) {
            data.service = mService;
            data.reservation = reservation;
            EventBus.getDefault().postSticky(data);
        }
        Intent intent = new Intent(this, BookingSelectionConfirmationActivity.class);
        startActivity(intent);
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        Slot slot;

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            final Calendar maxCal = Calendar.getInstance();
            maxCal.add(Calendar.DATE, 7);
            final Calendar minCal = Calendar.getInstance();
            minCal.add(Calendar.DATE, 1);
            c.set(Calendar.DAY_OF_WEEK, slot.day + 1);
            if (c.getTimeInMillis() < minCal.getTimeInMillis()) {
                c.add(Calendar.DATE, 7);
            }
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            DatePicker picker = dialog.getDatePicker();
            picker.setMinDate(c.getTimeInMillis());
            picker.setMaxDate(c.getTimeInMillis());
            return dialog;
        }

        public void setSlot(Slot s) {
            this.slot = s;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Reservation mReservation = new Reservation();
            mReservation.appointmentDate = String.format(Locale.getDefault(),
                    getString(R.string.appointmentdate_format_datepicker), year, month + 1, dayOfMonth);
            mReservation.service = slot.service;
            mReservation.category = slot.category;
            mReservation.slot = slot._id;
            EventBus.getDefault().post(new DateTimeSetEvent<>(mReservation));
        }
    }
}

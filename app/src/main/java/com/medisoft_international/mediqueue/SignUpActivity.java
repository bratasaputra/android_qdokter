package com.medisoft_international.mediqueue;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureModels.Patient;
import com.medisoft_international.mediqueue.VolleyRequest.SearchPatientRequest;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    EditText mName;
    EditText mAddress;
    EditText mPostCode;
    EditText mDob;
    EditText mPhone;
    CheckBox mMale;
    CheckBox mFemale;
    Button mConfirmButton;
    DatePickerDialog mDatePickerDialog;
    SimpleDateFormat mFormat;
    Date dob;
    ProgressBar mProgressBar;
//    protected Snackbar status;
//    protected String phonenumber = "+62";
    Toolbar mToolbar;
    Button mTnCButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Pendaftaran");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
//        Intent intent = getIntent();
//        if (intent.hasExtra("phonenumber")) {
//            phonenumber = intent.getStringExtra("phonenumber");
//        }
        Calendar today = Calendar.getInstance();
        today.add(Calendar.DAY_OF_MONTH, -1);
        mDatePickerDialog = new DatePickerDialog(this, this, today.get(Calendar.YEAR),
                today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH));
        mDatePickerDialog.getDatePicker().setMaxDate(today.getTimeInMillis());
        mFormat = new SimpleDateFormat("dd - MMM - yyyy", Locale.getDefault());
        mName = (EditText) findViewById(R.id.patient_name_entry);
        mAddress = (EditText) findViewById(R.id.patient_address_entry);
        mPostCode = (EditText) findViewById(R.id.patient_postalcode_entry);
        mPhone = (EditText) findViewById(R.id.patient_phone_entry);
        mDob = (EditText) findViewById(R.id.patient_dob_entry);
        mDob.setClickable(true);
        mDob.setFocusable(false);
        dob = today.getTime();
        mDob.setOnClickListener(this);
        mMale = (CheckBox) findViewById(R.id.patient_gender_male);
        mMale.setOnCheckedChangeListener(this);
        mFemale = (CheckBox) findViewById(R.id.patient_gender_female);
        mFemale.setOnCheckedChangeListener(this);
        mMale.setChecked(true);
        mConfirmButton = (Button) findViewById(R.id.sign_up_button);
        mConfirmButton.setOnClickListener(this);
        mProgressBar = (ProgressBar) findViewById(R.id.patient_signup_progressbar);
        mTnCButton = (Button) findViewById(R.id.tnc);
        mTnCButton.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        dob = calendar.getTime();
        mDob.setText(mFormat.format(dob));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tnc:
                String url = "http://www.medisoft-international.com/terms/TnC.html";
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setShowTitle(true);
                builder.setToolbarColor(ContextCompat.getColor(this, R.color.primary));
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(v.getContext(), Uri.parse(url));
                break;
            case R.id.patient_dob_entry:
                mDatePickerDialog.show();
                break;
            case R.id.sign_up_button:
                final Patient newPatient = new Patient();
                newPatient.name = mName.getText().toString();
                newPatient.address = mAddress.getText().toString();
                newPatient.postcode = mPostCode.getText().toString();
                newPatient.dob = mDob.getText().toString();
                newPatient.phone = Util.FormatPhoneNumber(mPhone.getText().toString());
                newPatient.male = mMale.isChecked();

                if(validatePatient(newPatient)) {
                    Map<String, String> searchParams = new HashMap<>();
                    searchParams.put("phoneNumber", newPatient.phone);
                    SearchPatientRequest request = new SearchPatientRequest(searchParams, new Response.Listener<List<Patient>>() {
                        @Override
                        public void onResponse(List<Patient> response) {
                            if (response.isEmpty()) {
                                // Phone number does not exist, go ahead and proceed to phone number authentication
                                Util.StickPatient(newPatient);
                                Intent startPhoneVerification = new Intent(SignUpActivity.this, PhoneVerificationActivity.class);
                                startActivity(startPhoneVerification);
                            } else {
                                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SignUpActivity.this);
                                dialogBuilder.setMessage("Either use another phone number or select login at the previous screen");
                                dialogBuilder.setTitle("This phone number has been registered");
                                dialogBuilder.setPositiveButton("Back", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                                dialogBuilder.setNegativeButton("Change phone number", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Do nothing, just dismiss the dialog
                                        dialog.dismiss();
                                        mPhone.requestFocus();
                                    }
                                });
                                dialogBuilder.setCancelable(false);
                                dialogBuilder.show();
//                            finish();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Util.Log(error);
                        }
                    });
                    Medisoft.getDefaultRequestQueue().add(request);
                }
                break;
        }
    }

    private boolean validatePatient(Patient patient) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        if (TextUtils.isEmpty(patient.name)) {
            builder.setTitle("Invalid Name");
            builder.setMessage("Name cannot be empty");
            builder.show();
            return false;
        }
        if (TextUtils.isEmpty(patient.address)) {
            builder.setTitle("Invalid Address");
            builder.setMessage("Address cannot be empty");
            builder.show();
            return false;
        }
        if (TextUtils.isEmpty(patient.postcode) || !TextUtils.isDigitsOnly(patient.postcode)) {
            builder.setTitle("Invalid Postcode");
            builder.setMessage("Please enter a valid post code");
            builder.show();
            return false;
        }
        if (TextUtils.isEmpty(patient.dob)) {
            builder.setTitle("Invalid Date of Birth");
            builder.setMessage("Please enter a valid date of birth");
            builder.show();
            return false;
        }
        if (TextUtils.isEmpty(patient.phone)) {
            builder.setTitle("Invalid Phone Number");
            builder.setMessage("Please provide a valid phone number");
            builder.show();
            return false;
        }
        return true;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.patient_gender_male:
                mFemale.setChecked(!isChecked);
                break;
            case R.id.patient_gender_female:
                mMale.setChecked(!isChecked);
                break;
        }
    }
}

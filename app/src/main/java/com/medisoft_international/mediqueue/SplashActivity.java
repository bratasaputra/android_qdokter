package com.medisoft_international.mediqueue;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureModels.Patient;
import com.medisoft_international.mediqueue.R;

import org.greenrobot.eventbus.EventBus;

public class SplashActivity extends AppCompatActivity {
    FirebaseAuth mAuth;
    Patient mPatient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mAuth = FirebaseAuth.getInstance();
        mPatient = Util.GetPatient(this);
        Util.StickPatient(mPatient);
        if (mAuth.getCurrentUser() == null || mPatient == null) {
            Intent showLanding = new Intent(this, LandingActivity.class);
            startActivity(showLanding);
        } else {
            Intent showMain = new Intent(this, MainActivity.class);
            startActivity(showMain);
        }
        finish();
    }
}

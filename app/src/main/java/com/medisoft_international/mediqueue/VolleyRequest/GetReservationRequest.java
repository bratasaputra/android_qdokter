package com.medisoft_international.mediqueue.VolleyRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.medisoft_international.mediqueue.AzureApiClient;
import com.medisoft_international.mediqueue.AzureModels.Reservation;
import com.medisoft_international.mediqueue.BuildConfig;
import com.medisoft_international.mediqueue.Util;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medisoft-Dev1 on 7/24/2017.
 */

public class GetReservationRequest extends Request<List<Reservation>> {
    private final Gson gson = new Gson();
    private final Response.Listener<List<Reservation>> listener;
    private final Map<String, String> searchParams;

    public GetReservationRequest(Map<String, String> searchParams, Response.Listener<List<Reservation>> listener,
                                 Response.ErrorListener errorListener) {
        super(Method.POST, AzureApiClient.SERVER_URL + "api/reservation/byPatient", errorListener);
        this.listener = listener;
        this.searchParams = searchParams;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", getBodyContentType());
        headers.put("Authorization", BuildConfig.BasicKey);
        return headers;
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy();
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return searchParams;
    }

    @Override
    protected Response<List<Reservation>> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Type listType = new TypeToken<List<Reservation>>() {}.getType();
            List<Reservation> reservationList = gson.fromJson(json, listType);
            return Response.success(reservationList, HttpHeaderParser.parseCacheHeaders(response));
        }
        catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
        catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(List<Reservation> response) {
        listener.onResponse(response);
    }
}

package com.medisoft_international.mediqueue.VolleyRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureApiClient;
import com.medisoft_international.mediqueue.AzureModels.Patient;
import com.medisoft_international.mediqueue.BuildConfig;
import com.medisoft_international.mediqueue.Util;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class PostNewPatientRequest extends Request<Patient> {
    /** Default charset for JSON request. */
    private static final String PROTOCOL_CHARSET = "utf-8";

    /** Content type for request. */
    private static final String PROTOCOL_CONTENT_TYPE =
            String.format("application/json; charset=%s", PROTOCOL_CHARSET);

    private final Gson gson = new Gson();
    private final Response.Listener<Patient> listener;
    private final Patient mPatient;

    public PostNewPatientRequest(Patient patient, Response.Listener<Patient> listener, Response.ErrorListener errorListener) {
        super(Method.POST, AzureApiClient.SERVER_URL + "api/patient", errorListener);
        this.listener = listener;
        this.mPatient = patient;
    }

    @Override
    protected Response<Patient> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            return Response.success(gson.fromJson(jsonString, Patient.class),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
             return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(Patient response) {
        listener.onResponse(response);
    }

    @Override
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", BuildConfig.BasicKey);
        return headers;
    }

    @Override
    public byte[] getBody() {
        try {
            return mPatient == null ? null : this.gson.toJson(mPatient).getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException uee) {
            Util.Log(uee);
            return null;
        }
    }
}

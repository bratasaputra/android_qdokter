package com.medisoft_international.mediqueue.VolleyRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medisoft_international.mediqueue.AzureApiClient;
import com.medisoft_international.mediqueue.AzureModels.Slot;
import com.medisoft_international.mediqueue.BuildConfig;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medisoft-Dev1 on 7/25/2017.
 */

public class SlotListRequest extends Request<List<Slot>> {
    private final Gson gson = new Gson();
    private final Response.Listener<List<Slot>> listener;

    public SlotListRequest(String serviceId, Response.Listener<List<Slot>> listener, Response.ErrorListener errorListener) {
        super(Method.GET, AzureApiClient.SERVER_URL + "api/slot/byservice/" + serviceId, errorListener);
        this.listener = listener;
    }

    @Override
    public Object getTag() {
        return SlotListRequest.class;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", BuildConfig.BasicKey);
        return headers;
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy();
    }

    @Override
    protected void deliverResponse(List<Slot> response) {
        this.listener.onResponse(response);
    }

    @Override
    protected Response<List<Slot>> parseNetworkResponse(NetworkResponse response) {
        String json = null;
        try {
            json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
        Type listType = new TypeToken<List<Slot>>() {}.getType();
        List<Slot> slots = gson.fromJson(json, listType);
        return Response.success(slots, HttpHeaderParser.parseCacheHeaders(response));
    }
}

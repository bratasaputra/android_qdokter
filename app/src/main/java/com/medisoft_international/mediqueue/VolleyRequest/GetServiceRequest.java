package com.medisoft_international.mediqueue.VolleyRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureApiClient;
import com.medisoft_international.mediqueue.AzureModels.Service;
import com.medisoft_international.mediqueue.BuildConfig;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Medisoft-Dev1 on 7/25/2017.
 */

public class GetServiceRequest extends Request<Service> {
    private final Gson gson = new Gson();
    private final Response.Listener<Service> listener;

    public GetServiceRequest(String id, Response.Listener<Service> listener, Response.ErrorListener errorListener) {
        super(Method.GET, AzureApiClient.SERVER_URL + "api/service/" + id, errorListener);
        this.listener = listener;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", BuildConfig.BasicKey);
        return headers;
    }

    @Override
    protected void deliverResponse(Service response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<Service> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(gson.fromJson(json, Service.class), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }
}

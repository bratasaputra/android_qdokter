package com.medisoft_international.mediqueue.VolleyRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.medisoft_international.mediqueue.AzureApiClient;
import com.medisoft_international.mediqueue.AzureModels.Patient;
import com.medisoft_international.mediqueue.BuildConfig;
import com.medisoft_international.mediqueue.Util;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.medisoft_international.mediqueue.AzureApiClient.DEFAULT_BACKOFF_MULT;
import static com.medisoft_international.mediqueue.AzureApiClient.DEFAULT_MAX_RETRIES;
import static com.medisoft_international.mediqueue.AzureApiClient.DEFAULT_TIMEOUT_MS;

/**
 * Created by Medisoft-Dev1 on 7/24/2017.
 */

public class SearchPatientRequest extends Request<List<Patient>> {

    private final Gson gson = new Gson();
    private final Response.Listener<List<Patient>> listener;
    private final Map<String, String> searchParams;

    public SearchPatientRequest(Map<String, String> searchParams,
                                Response.Listener<List<Patient>> listener,
                                Response.ErrorListener errorListener) {
        super(Method.POST, AzureApiClient.SERVER_URL + "api/patientsearch", errorListener);
        this.listener = listener;
        this.searchParams = searchParams;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", getBodyContentType());
        return headers;
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy(
                DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRIES, DEFAULT_BACKOFF_MULT);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return searchParams;
    }

    @Override
    protected void deliverResponse(List<Patient> response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<List<Patient>> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Type listType = new TypeToken<List<Patient>>() {}.getType();
            List<Patient> patientList = gson.fromJson(json, listType);
            return Response.success(patientList, HttpHeaderParser.parseCacheHeaders(response));
        }
        catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
        catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }
}

package com.medisoft_international.mediqueue.VolleyRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureApiClient;
import com.medisoft_international.mediqueue.AzureModels.Reservation;
import com.medisoft_international.mediqueue.BuildConfig;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Medisoft-Dev1 on 7/25/2017.
 */

public class MakeReservationRequest extends Request<Reservation> {
    private final Gson gson = new Gson();
    private final Reservation mReservation;
    private final Response.Listener<Reservation> listener;

    public MakeReservationRequest(Reservation reservation, Response.Listener<Reservation> listener, Response.ErrorListener errorListener) {
        super(Method.POST, AzureApiClient.SERVER_URL + "api/makereservationjson", errorListener);
        this.listener = listener;
        this.mReservation = reservation;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", BuildConfig.BasicKey);
        return headers;
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy(10000, 0, 1f); // No retry
    }

    @Override
    public String getBodyContentType() {
        return AzureApiClient.JSON_CONTENT_TYPE;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return mReservation == null ? null : gson.toJson(mReservation).getBytes(AzureApiClient.PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException e) {
            return super.getBody();
        }
    }

    @Override
    protected void deliverResponse(Reservation response) {
        this.listener.onResponse(response);
    }

    @Override
    protected Response<Reservation> parseNetworkResponse(NetworkResponse response) {
        String json = null;
        try {
            json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
        return Response.success(gson.fromJson(json, Reservation.class), HttpHeaderParser.parseCacheHeaders(response));
    }
}

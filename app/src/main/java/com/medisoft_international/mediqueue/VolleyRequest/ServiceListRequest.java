package com.medisoft_international.mediqueue.VolleyRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medisoft_international.mediqueue.AzureApiClient;
import com.medisoft_international.mediqueue.AzureModels.Service;
import com.medisoft_international.mediqueue.BuildConfig;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medisoft-Dev1 on 7/25/2017.
 */

public class ServiceListRequest extends Request<List<Service>> {
    private final Gson gson = new Gson();
    private final Map<String, String> searchParams;
    private final Response.Listener<List<Service>> listener;

    public ServiceListRequest(Map<String, String> searchParams, Response.Listener<List<Service>> listener, Response.ErrorListener errorListener) {
        super(Method.POST, AzureApiClient.SERVER_URL + "api/servicesearch", errorListener);
        this.listener = listener;
        this.searchParams = searchParams;
    }

    @Override
    public Object getTag() {
        return ServiceListRequest.class;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", BuildConfig.BasicKey);
        return headers;
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy();
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return this.searchParams;
    }

    @Override
    protected void deliverResponse(List<Service> response) {
        this.listener.onResponse(response);
    }

    @Override
    protected Response<List<Service>> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Type listType = new TypeToken<List<Service>>() {}.getType();
            List<Service> services = gson.fromJson(json, listType);
            return Response.success(services, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }
}

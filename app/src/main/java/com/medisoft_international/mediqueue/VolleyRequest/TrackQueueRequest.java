package com.medisoft_international.mediqueue.VolleyRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureApiClient;
import com.medisoft_international.mediqueue.AzureModels.Queue;
import com.medisoft_international.mediqueue.BuildConfig;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Medisoft-Dev1 on 7/25/2017.
 */

public class TrackQueueRequest extends Request<Queue.QueueCount> {
    private final Gson gson = new Gson();
    private final Response.Listener<Queue.QueueCount> listener;
    public TrackQueueRequest(String id, Response.Listener<Queue.QueueCount> listener, Response.ErrorListener errorListener) {
        super(Method.GET, AzureApiClient.SERVER_URL + "api/track/remaining/" + id, errorListener);
        this.listener = listener;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", BuildConfig.BasicKey);
        return headers;
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy();
    }

    @Override
    protected Response<Queue.QueueCount> parseNetworkResponse(NetworkResponse response) {
        String json = null;
        try {
            switch (response.statusCode) {
                case 200:
                    json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
                    return Response.success(gson.fromJson(json, Queue.QueueCount.class), HttpHeaderParser.parseCacheHeaders(response));
                default:
                    return Response.success(null, HttpHeaderParser.parseCacheHeaders(response));
            }
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(Queue.QueueCount response) {
        this.listener.onResponse(response);
    }
}

package com.medisoft_international.mediqueue.VolleyRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medisoft_international.mediqueue.AzureApiClient;
import com.medisoft_international.mediqueue.AzureModels.Specialisation;
import com.medisoft_international.mediqueue.BuildConfig;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medisoft-Dev1 on 7/25/2017.
 */

public class GetSpesialisListRequest extends Request<List<Specialisation>> {
    private final Gson gson = new Gson();
    private final Response.Listener<List<Specialisation>> listener;

    public GetSpesialisListRequest(Response.Listener<List<Specialisation>> listener, Response.ErrorListener errorListener)
    {
        super(Method.GET, AzureApiClient.SERVER_URL + "api/spesialis", errorListener);
        this.listener = listener;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", BuildConfig.BasicKey);
        return headers;
    }

    @Override
    public String getBodyContentType() {
        return "application/json; charset=utf-8";
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        // Timeout 5 seconds
        // Retry 3x
        // Backoff 1.5x
        return new DefaultRetryPolicy(5000, 3, 1.5f);
    }

    @Override
    protected Response<List<Specialisation>> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Type listType = new TypeToken<List<Specialisation>>() {}.getType();
            List<Specialisation> result = gson.fromJson(json, listType);
            return Response.success(result, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(List<Specialisation> response) {
        listener.onResponse(response);
    }
}

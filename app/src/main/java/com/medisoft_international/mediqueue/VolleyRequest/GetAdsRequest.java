package com.medisoft_international.mediqueue.VolleyRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.medisoft_international.mediqueue.AzureApiClient;
import com.medisoft_international.mediqueue.AzureModels.Ads;
import com.medisoft_international.mediqueue.BuildConfig;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Medisoft-Dev1 on 7/25/2017.
 */

public class GetAdsRequest extends Request<Ads> {
    private final Gson gson = new Gson();
    private final Response.Listener<Ads> listener;

    public GetAdsRequest(Response.Listener<Ads> listener, Response.ErrorListener errorListener) {
        super(Method.GET, AzureApiClient.SERVER_URL + "api/ads", errorListener);
        this.listener = listener;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", BuildConfig.BasicKey);
        return headers;
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy();
    }

    @Override
    protected Response<Ads> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(gson.fromJson(json, Ads.class), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(Ads response) {
        this.listener.onResponse(response);
    }
}

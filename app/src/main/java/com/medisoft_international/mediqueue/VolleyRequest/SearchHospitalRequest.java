package com.medisoft_international.mediqueue.VolleyRequest;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.medisoft_international.mediqueue.AzureApiClient;
import com.medisoft_international.mediqueue.AzureModels.Hospital;
import com.medisoft_international.mediqueue.BuildConfig;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Medisoft-Dev1 on 7/25/2017.
 */

public class SearchHospitalRequest extends Request<List<Hospital>> {
    private final Gson gson = new Gson();
    private final Response.Listener<List<Hospital>> listener;
    private final Map<String, String> searchParams;

    public SearchHospitalRequest(Map<String, String> searchParams, Response.Listener<List<Hospital>> listener, Response.ErrorListener errorListener) {
        super(Method.POST, AzureApiClient.SERVER_URL + "api/hospitalsearch", errorListener);
        this.listener = listener;
        this.searchParams = searchParams;
    }

    @Override
    public Object getTag() {
        return SearchHospitalRequest.class;
    }

    @Override
    public RetryPolicy getRetryPolicy() {
        return new DefaultRetryPolicy();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", BuildConfig.BasicKey);
        return headers;
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return this.searchParams;
    }

    @Override
    public String getBodyContentType() {
        return super.getBodyContentType();
    }

    @Override
    protected void deliverResponse(List<Hospital> response) {
        this.listener.onResponse(response);
    }

    @Override
    protected Response<List<Hospital>> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            Type listType = new TypeToken<List<Hospital>>() {}.getType();
            List<Hospital> hospitals = this.gson.fromJson(json, listType);
            return Response.success(hospitals, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }
}
